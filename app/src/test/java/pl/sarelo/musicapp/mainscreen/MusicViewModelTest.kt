package pl.sarelo.musicapp.mainscreen

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import pl.sarelo.data.network.ConnectivityException
import pl.sarelo.data.network.api.RetrofitException
import pl.sarelo.domain.entities.MusicResponse
import pl.sarelo.domain.usecases.SearchMusicUseCase

class MusicViewModelTest {

    @get:Rule
    val testRule = InstantTaskExecutorRule()

    private lateinit var musicViewModel: MusicViewModel
    private lateinit var searchMusicUseCase: SearchMusicUseCase

    @Before
    fun setup() {

        searchMusicUseCase = mockk(relaxed = true)

        every {
            searchMusicUseCase.execute(any())
        } returns Single.just(MusicResponse())

        musicViewModel = MusicViewModel(searchMusicUseCase)
    }

    @Test
    fun `check if user click on local filter then proper event will be propagated`() {
        // Given
        val testObserver = musicViewModel.sourceLiveData
            .test()
            .assertHasValue()
            .assertHistorySize(1)
            .assertValue(SourceType.BOTH)

        // When
        musicViewModel.setSource(SourceType.LOCAL)

        // Then
        testObserver
            .awaitValue()
            .assertHasValue()
            .assertHistorySize(2)
            .assertValue(SourceType.LOCAL)
    }

    @Test
    fun `when ConnectivityException came from usecase Error_NoInternet event should be send`() {
        // Given
        every {
            searchMusicUseCase.execute(any())
        } returns Single.just(
            MusicResponse(
                emptyList(),
                errorNetwork = RetrofitException.errorHandleInApp(ConnectivityException())
            )
        )

        musicViewModel = MusicViewModel(searchMusicUseCase)

        val testObserver = musicViewModel.events
            .test()
            .assertNoValue()
            .assertHistorySize(0)

        // When
        musicViewModel.onSearchClicked()
        val result = testObserver.valueHistory()

        // Then
        testObserver
            .awaitValue()
            .assertHasValue()
            .assertHistorySize(3)
            .assertValue { event ->
                event is MusicFragment.Event.NewResult
            }

        Assert.assertTrue(result.contains(MusicFragment.Event.Error.NoInternet))
    }

    @Test
    fun `when user scroll to end of list should be called search and NextPageResult event should be send`() {
        // Given
        val testObserver = musicViewModel.events
            .test()
            .assertNoValue()
            .assertHistorySize(0)
        musicViewModel.onSearchClicked()

        // When
        musicViewModel.loadNextPage()

        // Then
        verify(exactly = 2) { searchMusicUseCase.execute(any()) }

        testObserver
            .awaitValue()
            .assertHasValue()
            .assertHistorySize(4)
            .assertValue { event ->
                event is MusicFragment.Event.NextPageResult
            }
    }
}
