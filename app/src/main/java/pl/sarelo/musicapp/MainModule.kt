package pl.sarelo.musicapp

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import pl.sarelo.data.local.DataRepository
import pl.sarelo.data.local.json.JsonDataRepository
import pl.sarelo.data.local.model.LocalMusicData
import pl.sarelo.data.local.repositories.LocalMusicRepository
import pl.sarelo.data.network.ConnectionProvider
import pl.sarelo.data.network.api.ApiFactory
import pl.sarelo.data.network.api.ConnectivityInterceptor
import pl.sarelo.data.network.api.HttpClientProvider
import pl.sarelo.data.network.api.NetworkApiFactory
import pl.sarelo.data.network.repositories.NetworkMusicRepository
import pl.sarelo.data.network.requests.SearchRequest
import pl.sarelo.domain.SchedulerProvider
import pl.sarelo.domain.repositories.MusicRepository
import pl.sarelo.domain.usecases.SearchMusicUseCase
import pl.sarelo.musicapp.common.connectivity.InternetConnectionProvider
import pl.sarelo.musicapp.mainscreen.MusicViewModel

private const val iTunesUrl = "https://itunes.apple.com"
private const val songFileName: String = "songs-list.json"

val appModule = module {

    single<SchedulerProvider> {
        object : SchedulerProvider {
            override fun io() = Schedulers.io()
            override fun mainThread() = AndroidSchedulers.mainThread()
        }
    }

    single { Moshi.Builder().add(KotlinJsonAdapterFactory()).build() }

    single<ConnectionProvider> { InternetConnectionProvider() }

    single { ConnectivityInterceptor(get()) }

    single { HttpClientProvider(get(), BuildConfig.DEBUG.not()).buildOkHttpClient() }

    single<ApiFactory> { NetworkApiFactory(iTunesUrl, get()) }

    single { get<ApiFactory>().createRequest(SearchRequest::class.java) }

    single<MusicRepository>(named("network")) { NetworkMusicRepository(get()) }

    single<DataRepository<LocalMusicData>> {
        JsonDataRepository(androidContext().assets.open(songFileName), get(), LocalMusicData::class.java)
    }

    single<MusicRepository>(named("local")) { LocalMusicRepository(get()) }

    single { SearchMusicUseCase(get(named("network")), get(named("local")), get()) }

    viewModel { MusicViewModel(get()) }
}
