package pl.sarelo.musicapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import pl.sarelo.musicapp.mainscreen.MusicFragment

private const val FRAGMENT_TAG: String = "FRAGMENT_TAG"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(supportFragmentManager) {
            if (findFragmentByTag(FRAGMENT_TAG) == null) {
                beginTransaction().replace(R.id.container, MusicFragment.create(), FRAGMENT_TAG).commit()
            }
        }
    }
}
