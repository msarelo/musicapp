package pl.sarelo.musicapp.mainscreen.musiclist

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.sarelo.musicapp.mainscreen.ITEMS_PER_PAGE

abstract class LimitedScrollListener(private val linearLayoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {

    private var currentPage = 1

    abstract fun onLoadMore()

    fun reset() {
        currentPage = 1
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (hasReachedEdge(linearLayoutManager)) {
            currentPage++
            onLoadMore()
        }
    }

    private fun hasReachedEdge(linearLayoutManager: LinearLayoutManager): Boolean {
        with(linearLayoutManager) {
            val visibleItemCount = childCount
            val totalItemCount = itemCount
            val firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition()

            return (visibleItemCount + firstVisibleItemPosition >= totalItemCount &&
                    firstVisibleItemPosition >= 0 &&
                    totalItemCount >= ITEMS_PER_PAGE)
        }
    }
}
