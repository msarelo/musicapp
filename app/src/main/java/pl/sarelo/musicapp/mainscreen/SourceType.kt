package pl.sarelo.musicapp.mainscreen

enum class SourceType {
    BOTH,
    LOCAL,
    NETWORK
}
