package pl.sarelo.musicapp.mainscreen.musiclist

import pl.sarelo.musicapp.R
import pl.sarelo.musicapp.common.databinding.BaseAdapter

class MusicAdapter(val onClick: (MusicContentViewData) -> Unit) : BaseAdapter() {

    var items: MutableList<MusicContentViewData> = mutableListOf()

    override fun getObjForPosition(position: Int) = items[position]

    override fun getLayoutIdForPosition(position: Int) = R.layout.music_content_item

    override fun getItemCount() = items.size

    override fun onItemClick(position: Int) {
        onClick(items[position])
    }

    fun setNewData(newData: List<MusicContentViewData>) {
        items.clear()
        items.addAll(newData)
        notifyDataSetChanged()
    }

    fun addData(newData: List<MusicContentViewData>) {
        items.addAll(newData)
        notifyDataSetChanged()
    }

    fun clearData() {
        this.items.clear()
        notifyDataSetChanged()
    }
}
