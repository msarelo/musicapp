package pl.sarelo.musicapp.mainscreen.musiclist

data class MusicContentViewData(val index: Int, val songName: String, val artistName: String, val releaseDate: String)
