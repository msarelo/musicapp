package pl.sarelo.musicapp.mainscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.fragment_music.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import pl.sarelo.musicapp.R
import pl.sarelo.musicapp.common.extensions.hideKeyboard
import pl.sarelo.musicapp.databinding.FragmentMusicBinding
import pl.sarelo.musicapp.mainscreen.musiclist.LimitedScrollListener
import pl.sarelo.musicapp.mainscreen.musiclist.MusicAdapter
import pl.sarelo.musicapp.mainscreen.musiclist.MusicContentViewData

class MusicFragment : Fragment() {

    private val musicViewModel: MusicViewModel by viewModel()
    private var binding: FragmentMusicBinding? = null

    private lateinit var musicAdapter: MusicAdapter
    private lateinit var limitedScrollListener: LimitedScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        musicAdapter = MusicAdapter {
            Toast.makeText(context, it.songName, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentMusicBinding>(
            inflater,
            R.layout.fragment_music,
            container,
            false
        )
            .apply {
                lifecycleOwner = this@MusicFragment
                viewModel = musicViewModel
            }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initRecycler()

        searchInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchInput.hideKeyboard()
                musicViewModel.onSearchClicked()
                return@setOnEditorActionListener true
            }
            false
        }

        with(musicViewModel.events) {
            value?.let { handleEvent(it) }
            observe(this@MusicFragment, Observer { handleEvent(it) })
        }
    }

    private fun initRecycler() {
        with(resultList) {
            val manager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            layoutManager = manager
            adapter = musicAdapter

            clearOnScrollListeners()
            limitedScrollListener = object : LimitedScrollListener(manager) {
                override fun onLoadMore() {
                    musicViewModel.loadNextPage()
                }
            }
            addOnScrollListener(limitedScrollListener)
        }
    }

    private fun handleEvent(event: Event?) {
        Logger.d("event: $event")
        when (event) {
            Event.User.Clear -> {
                musicAdapter.clearData()
                limitedScrollListener.reset()
            }
            is Event.NewResult -> musicAdapter.setNewData(event.data)
            is Event.NextPageResult -> musicAdapter.addData(event.data)
            Event.Error.ApiLimitReached -> Toast.makeText(context, R.string.api_error, Toast.LENGTH_LONG).show()
            Event.Error.LocalError -> Toast.makeText(context, R.string.local_error, Toast.LENGTH_LONG).show()
            Event.Error.EmptyData -> Toast.makeText(context, R.string.empty_error, Toast.LENGTH_LONG).show()
            Event.Error.NoInternet -> Toast.makeText(context, R.string.internet_error, Toast.LENGTH_LONG).show()
        }
    }

    companion object {
        fun create() = MusicFragment()
    }

    sealed class Event {

        data class NewResult(var data: List<MusicContentViewData>) : Event()
        data class NextPageResult(var data: List<MusicContentViewData>) : Event()
        data class InternetState(var connected: Boolean) : Event()

        sealed class User : Event() {
            object Clear : Event()
            object Searching : Event()
        }

        sealed class Error : Event() {
            object NoInternet : Event()
            object ApiLimitReached : Event()
            object LocalError : Event()
            object EmptyData : Event()
        }
    }
}
