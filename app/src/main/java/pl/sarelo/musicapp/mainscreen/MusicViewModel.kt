package pl.sarelo.musicapp.mainscreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.orhanobut.logger.Logger
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import pl.sarelo.data.network.api.RetrofitException
import pl.sarelo.data.network.api.RetrofitException.Kind
import pl.sarelo.domain.entities.MusicContent
import pl.sarelo.domain.entities.MusicResponse
import pl.sarelo.domain.entities.SearchRequest
import pl.sarelo.domain.usecases.SearchMusicUseCase
import pl.sarelo.musicapp.common.connectivity.ConnectivityReceiver
import pl.sarelo.musicapp.common.extensions.debounce
import pl.sarelo.musicapp.mainscreen.musiclist.MusicContentViewData

const val ITEMS_PER_PAGE = 10

private const val TEXT_TYPE_DELAY = 500L
private const val MIN_CHARS_TO_SEARCH = 3
private const val START_PAGE = 0
private const val HTTP_FORBIDDEN = 403
private const val EMPTY_STRING = ""

class MusicViewModel(private val searchMusicUseCase: SearchMusicUseCase) : ViewModel() {

    var events = MutableLiveData<MusicFragment.Event>()
    var progressBarVisible = MutableLiveData<Boolean>()
    var contentVisible = MutableLiveData<Boolean>()
    var offlineVisible = MutableLiveData<Boolean>()
    var searchTerm = MutableLiveData<String>()
    var sourceLiveData = MutableLiveData<SourceType>().apply {
        value = SourceType.BOTH
    }

    private var searchDisposable: Disposable? = null
    private var internetDisposable: Disposable? = null
    private var currentPage = START_PAGE
    private var loading = false

    init {
        events.observeForever {
            when (it) {
                is MusicFragment.Event.InternetState -> {
                    contentVisible.value = it.connected
                    offlineVisible.value = !it.connected
                    progressBarVisible.value = false
                }
                MusicFragment.Event.User.Searching -> progressBarVisible.value = true
                else -> {
                    progressBarVisible.value = false
                }
            }
        }

        internetDisposable = ConnectivityReceiver.NETWORK_BEHAVIOR_SUBJECT
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy { connected ->
                events.postValue(MusicFragment.Event.InternetState(connected))

                if (connected) {
                    if (sourceLiveData.hasObservers().not()) {
                        sourceLiveData.observeForever {
                            onSearchClicked()
                        }

                        // Small hack to avoid crete boolean flag for initialisation of each live data observer
                        searchTerm.debounce(TEXT_TYPE_DELAY).observeForever { searchTerm ->
                            if (searchTerm.canBeProcessed()) {
                                onSearchClicked()
                            }
                        }
                    }
                }
            }
    }

    fun onSearchClicked() {
        currentPage = START_PAGE
        loading = true
        doSearch()
    }

    fun setSource(sourceType: SourceType) {
        sourceLiveData.value = sourceType
    }

    private fun doSearch() {
        sourceLiveData.value?.let { source ->
            when (source) {
                SourceType.BOTH -> SearchRequest.Both(getSearchTerm(), currentPage, ITEMS_PER_PAGE)
                SourceType.LOCAL -> SearchRequest.Local(getSearchTerm(), currentPage, ITEMS_PER_PAGE)
                SourceType.NETWORK -> SearchRequest.Network(getSearchTerm(), currentPage, ITEMS_PER_PAGE)
            }.also { searchRequest ->
                sendRequest(searchRequest)
            }
        }
    }

    private fun sendRequest(searchRequest: SearchRequest) {
        events.postValue(MusicFragment.Event.User.Searching)

        searchDisposable?.dispose()
        searchDisposable = searchMusicUseCase.execute(searchRequest)
            .map { musicResponse ->
                checkErrors(musicResponse)
                return@map musicResponse.mapToViewData()
            }
            .subscribeBy {
                loading = false
                if (currentPage == START_PAGE) {
                    events.postValue(MusicFragment.Event.NewResult(it))
                } else {
                    events.postValue(MusicFragment.Event.NextPageResult(it))
                }
            }
    }

    private fun getSearchTerm() = searchTerm.value.orEmpty()

    private fun checkErrors(it: MusicResponse) {
        when {
            it.errorNetwork != null -> checkNetworkError(it.errorNetwork!!)
            it.errorLocal != null -> {
                Logger.e(it.errorLocal, "Local Error: ")
                events.value = MusicFragment.Event.Error.LocalError
            }
        }
    }

    private fun checkNetworkError(error: Throwable) {
        Logger.e(error, "Network error : ")
        when (error) {
            is RetrofitException -> {
                when {
                    error.kind == Kind.HANDLE_IN_APP -> events.value = MusicFragment.Event.Error.NoInternet
                    error.httpStatusCode == HTTP_FORBIDDEN -> events.value = MusicFragment.Event.Error.ApiLimitReached
                }
            }
            is IllegalStateException -> events.value = MusicFragment.Event.Error.EmptyData
        }
    }

    fun onClearClicked() {
        loading = false
        searchDisposable?.dispose()
        searchTerm.postValue(EMPTY_STRING)
        currentPage = START_PAGE
        events.postValue(MusicFragment.Event.User.Clear)
    }

    fun loadNextPage() {
        if (loading.not()) {
            currentPage++
            doSearch()
        }
    }

    override fun onCleared() {
        searchDisposable?.dispose()
        internetDisposable?.dispose()
        super.onCleared()
    }
}

private fun String.canBeProcessed() = trim().length >= MIN_CHARS_TO_SEARCH

private fun MusicResponse.mapToViewData() =
    data.asSequence()
        .mapIndexed { index: Int, musicContent: MusicContent -> musicContent.toViewData(index) }
        .toList()

private fun MusicContent.toViewData(index: Int) =
    MusicContentViewData(index, artistInfo.artistName, title, releaseInfo.releaseDate)
