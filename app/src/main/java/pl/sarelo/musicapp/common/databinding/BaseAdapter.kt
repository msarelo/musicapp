package pl.sarelo.musicapp.common.databinding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import pl.sarelo.musicapp.BR

abstract class BaseAdapter : RecyclerView.Adapter<BaseViewHolder>(), OnItemClickListener {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        BaseViewHolder(inflateBindableView(parent, viewType))

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        getObjForPosition(position).also { holder.bind(it, this) }
    }

    override fun getItemViewType(position: Int) = getLayoutIdForPosition(position)

    protected abstract fun getObjForPosition(position: Int): Any

    protected abstract fun getLayoutIdForPosition(position: Int): Int

    private fun inflateBindableView(parent: ViewGroup, viewType: Int): ViewDataBinding {
        return DataBindingUtil.inflate(LayoutInflater.from(parent.context), viewType, parent, false)
    }
}

open class BaseViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(viewData: Any, clickListener: OnItemClickListener) {
        bindHolder(binding, viewData, clickListener, adapterPosition)
    }

    private fun bindHolder(
        binding: ViewDataBinding,
        viewData: Any,
        clickListener: OnItemClickListener,
        position: Int? = null
    ) =
        with(binding) {
            setVariable(BR.viewData, viewData)
            setVariable(BR.clickListener, clickListener)
            position?.let { setVariable(BR.position, it) }
            executePendingBindings()
        }
}
