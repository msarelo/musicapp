package pl.sarelo.musicapp.common.connectivity

import android.annotation.SuppressLint
import io.reactivex.schedulers.Schedulers
import pl.sarelo.data.network.ConnectionProvider

@SuppressLint("CheckResult")
class InternetConnectionProvider : ConnectionProvider {

    private var isConnectedToInternet = false

    init {
        ConnectivityReceiver.NETWORK_BEHAVIOR_SUBJECT
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe { connected ->
                isConnectedToInternet = connected
            }
    }

    override val isConnected: Boolean
        get() = isConnectedToInternet
}
