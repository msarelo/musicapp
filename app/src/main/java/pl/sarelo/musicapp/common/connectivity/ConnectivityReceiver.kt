package pl.sarelo.musicapp.common.connectivity

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.orhanobut.logger.Logger
import io.reactivex.subjects.BehaviorSubject

class ConnectivityReceiver : BroadcastReceiver() {

    companion object {
        val NETWORK_BEHAVIOR_SUBJECT = BehaviorSubject.create<Boolean>()
    }

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context, intent: Intent) {
        NETWORK_BEHAVIOR_SUBJECT.onNext(isConnected(context))
    }

    private fun isConnected(context: Context): Boolean {
        return try {
            context.getSystemService(Context.CONNECTIVITY_SERVICE)?.let {
                (it as ConnectivityManager).activeNetworkInfo?.isConnected ?: false
            } ?: false
        } catch (e: RuntimeException) {
            Logger.e(e, "Error during obtain network status")
            false
        }
    }
}
