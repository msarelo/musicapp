package pl.sarelo.musicapp.common.databinding

interface OnItemClickListener {
    fun onItemClick(position: Int)
}
