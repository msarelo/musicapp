package pl.sarelo.musicapp

import android.app.Application
import android.content.IntentFilter
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.sarelo.musicapp.common.connectivity.ConnectivityReceiver

private const val CONNECTIVITY_INTENT_FILTER = "android.net.conn.CONNECTIVITY_CHANGE"
private const val LOG_TAG = "LOG"

class MusicApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initLogger()
        registerConnectivityReceiver()

        startKoin {
            androidContext(this@MusicApp)
            modules(appModule)
        }
    }

    private fun initLogger() {
        val formatStrategy = PrettyFormatStrategy.newBuilder()
            .showThreadInfo(true)
            .tag(LOG_TAG)
            .build()

        Logger.addLogAdapter(object : AndroidLogAdapter(formatStrategy) {
            override fun isLoggable(priority: Int, tag: String?): Boolean {
                return BuildConfig.DEBUG
            }
        })
    }

    private fun registerConnectivityReceiver() {
        registerReceiver(ConnectivityReceiver(), IntentFilter(CONNECTIVITY_INTENT_FILTER))
    }
}
