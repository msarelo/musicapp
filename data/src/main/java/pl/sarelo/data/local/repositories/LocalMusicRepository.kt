package pl.sarelo.data.local.repositories

import androidx.annotation.IntRange
import io.reactivex.Single
import pl.sarelo.data.local.DataRepository
import pl.sarelo.data.local.mapper.toMusicContent
import pl.sarelo.data.local.model.LocalMusicData
import pl.sarelo.domain.entities.MusicContent
import pl.sarelo.domain.repositories.MusicRepository

class LocalMusicRepository(private val dataRepository: DataRepository<LocalMusicData>) : MusicRepository {

    override fun search(searchTerm: String, @IntRange(from = 0) pageNumber: Int, @IntRange(from = 1) itemsPerPage: Int): Single<List<MusicContent>> {
        val startIndex = getStartIndex(pageNumber, itemsPerPage)
        val endIndex = getEndIndex(pageNumber, itemsPerPage)
        return dataRepository.getAllData()
            .map { list ->
                filterListBySearchTerm(list, searchTerm)
                    .subListSafe(startIndex, endIndex + 1)
                    .asSequence()
                    .map { it.toMusicContent() }
                    .toList()
            }
    }

    private fun filterListBySearchTerm(list: List<LocalMusicData>, searchTerm: String): List<LocalMusicData> {
        return list.asSequence()
            .filter { it.containString(searchTerm) }
            .toList()
    }
}

private fun <E> List<E>.subListSafe(startIndex: Int, endIndex: Int): List<E> {
    if (size == 0) {
        return this
    }
    val end = if (endIndex > size) {
        size
    } else {
        maxOf(0, minOf(endIndex, size))
    }
    return subList(startIndex, end)
}
