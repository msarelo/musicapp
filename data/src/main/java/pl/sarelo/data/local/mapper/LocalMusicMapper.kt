package pl.sarelo.data.local.mapper

import pl.sarelo.data.local.model.LocalMusicData
import pl.sarelo.domain.entities.ArtistInfo
import pl.sarelo.domain.entities.MusicContent
import pl.sarelo.domain.entities.ReleaseInfo

fun LocalMusicData.toMusicContent(): MusicContent {
    val artistInfo = ArtistInfo(artistClean.orEmpty())

    // TODO convert date to common format
    val releaseInfo = ReleaseInfo(releaseYear.toString())

    return MusicContent(songClean.orEmpty(), "", artistInfo, releaseInfo)
}
