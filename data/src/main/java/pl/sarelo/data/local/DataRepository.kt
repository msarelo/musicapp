package pl.sarelo.data.local

import io.reactivex.Single

interface DataRepository<T> {
    fun getAllData(): Single<List<T>>
}
