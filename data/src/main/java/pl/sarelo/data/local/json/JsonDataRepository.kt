package pl.sarelo.data.local.json

import com.orhanobut.logger.Logger
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.reactivex.Single
import okio.Okio
import pl.sarelo.data.local.DataRepository
import java.io.InputStream
import java.lang.reflect.ParameterizedType
import kotlin.system.measureTimeMillis

class JsonDataRepository<T>(json: InputStream, moshi: Moshi, clazz: Class<T>) : DataRepository<T> {

    private val data = lazy {
        json.use {
            Okio.source(it).use {
                Okio.buffer(it).use { bufferedSource ->
                    val type: ParameterizedType = Types.newParameterizedType(List::class.java, clazz)
                    val adapter: JsonAdapter<List<T>> = moshi.adapter(type)
                    return@lazy adapter.fromJson(bufferedSource)
                }
            }
        }
    }

    override fun getAllData(): Single<List<T>> = Single.create<List<T>> { emitter ->
        measureTimeMillis {
            data.value?.let {
                emitter.onSuccess(it)
            } ?: emitter.onError(IllegalStateException("Can't parse JSON"))
        }.also {
            Logger.d("execution time in ms : $it")
        }
    }
}
