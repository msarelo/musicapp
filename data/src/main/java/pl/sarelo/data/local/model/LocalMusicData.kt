package pl.sarelo.data.local.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LocalMusicData(
    @Json(name = "ARTIST CLEAN")
    var artistClean: String?,
    @Json(name = "COMBINED")
    var combined: String?,
    @Json(name = "F*G")
    var fG: Int?,
    @Json(name = "First?")
    var first: Int?,
    @Json(name = "PlayCount")
    var playCount: Int?,
    @Json(name = "Release Year")
    var releaseYear: String?,
    @Json(name = "Song Clean")
    var songClean: String?,
    @Json(name = "Year?")
    var year: Int?
) {

    fun containString(searchTerm: String): Boolean {
        return if (artistClean == null && songClean == null && (year == null || year == 0)) {
            false
        } else {
            artistClean.safeContains(searchTerm) ||
                    songClean.safeContains(searchTerm) ||
                    releaseYear.safeContains(searchTerm)
        }
    }

    private fun String?.safeContains(searchTerm: String): Boolean {
        return when {
            this == null -> false
            else -> toLowerCase().contains(searchTerm.toLowerCase())
        }
    }
}
