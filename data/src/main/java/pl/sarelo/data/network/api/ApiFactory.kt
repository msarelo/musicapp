package pl.sarelo.data.network.api

interface ApiFactory {
    fun <T> createRequest(serviceClass: Class<T>): T
}
