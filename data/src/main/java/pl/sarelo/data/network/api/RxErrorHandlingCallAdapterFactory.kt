package pl.sarelo.data.network.api

import io.reactivex.Single
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.lang.reflect.Type

// https://stackoverflow.com/questions/43225556/how-to-make-rxerrorhandlingcalladapterfactory
class RxErrorHandlingCallAdapterFactory : CallAdapter.Factory() {

    private val original: RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<Any, Single<Any>> {
        return RxCallAdapterWrapper(original.get(returnType, annotations, retrofit) as CallAdapter<Any, Single<Any>>)
    }

    private class RxCallAdapterWrapper internal constructor(private val wrapped: CallAdapter<Any, Single<Any>>) :
        CallAdapter<Any, Single<Any>> {

        override fun responseType(): Type {
            return wrapped.responseType()
        }

        override fun adapt(call: Call<Any>): Single<Any> {
            return (wrapped.adapt(call) as Single<Any>)
                .onErrorResumeNext { throwable -> ExceptionHelper.asRetrofitException(throwable) }
        }
    }
}
