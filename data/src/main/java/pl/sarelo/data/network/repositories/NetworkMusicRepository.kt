package pl.sarelo.data.network.repositories

import io.reactivex.Single
import pl.sarelo.data.network.mapper.toMusicContent
import pl.sarelo.data.network.requests.SearchRequest
import pl.sarelo.domain.entities.MusicContent
import pl.sarelo.domain.repositories.MusicRepository
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Locale

class NetworkMusicRepository(private val searchRequest: SearchRequest) : MusicRepository {

    private val inDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    private val outDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())

    private val dateFormatter: (date: String?) -> String = {
        try {
            outDate.format(inDate.parse(it))
        } catch (e: ParseException) {
            it.orEmpty()
        }
    }

    override fun search(searchTerm: String, pageNumber: Int, itemsPerPage: Int): Single<List<MusicContent>> {
        val offset = maxOf(((pageNumber * itemsPerPage) - 1), 0)
        val queryMap = mutableMapOf(
            "term" to searchTerm,
            "entity" to "song",
            "limit" to itemsPerPage.toString(),
            "sort" to "top",
            "offset" to offset.toString()
        )

        return searchRequest.searchResult(queryMap)
            .flatMap {
                if (it.results.isNullOrEmpty().not()) {
                    Single.just(it.results?.filterNotNull())
                } else {
                    Single.error(IllegalStateException("missing data"))
                }
            }
            .toObservable()
            .flatMapIterable { it }
            .map {
                it.toMusicContent(dateFormatter)
            }.toList()
    }
}
