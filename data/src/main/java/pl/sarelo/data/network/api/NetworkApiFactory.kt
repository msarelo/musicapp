package pl.sarelo.data.network.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class NetworkApiFactory(private val baseUrl: String, private val okHttpClient: OkHttpClient) : ApiFactory {

    private var retrofit = buildRestClient()

    override fun <T> createRequest(serviceClass: Class<T>): T = retrofit.create(serviceClass)

    private fun buildRestClient() = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory())
        .build()
}
