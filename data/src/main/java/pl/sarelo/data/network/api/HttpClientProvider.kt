package pl.sarelo.data.network.api

import com.orhanobut.logger.Logger
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

private const val DEFAULT_TIMEOUT = 30L

class HttpClientProvider(private val connectivityInterceptor: ConnectivityInterceptor, isRelease: Boolean) {

    private val loggingLevel = getLoggingLevel(isRelease)

    private fun getLoggingLevel(isRelease: Boolean): HttpLoggingInterceptor.Level {
        return if (isRelease) {
            HttpLoggingInterceptor.Level.NONE
        } else {
            HttpLoggingInterceptor.Level.HEADERS
        }
    }

    fun buildOkHttpClient(): OkHttpClient = buildBaseClient()
        .addInterceptor(buildHttpLoggingInterceptor())
        .addInterceptor(connectivityInterceptor).build()

    private fun buildBaseClient() = OkHttpClient()
        .newBuilder()
        .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)

    private fun buildHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Logger.d("OkHttp: $it")
        })
        loggingInterceptor.level = loggingLevel
        return loggingInterceptor
    }
}
