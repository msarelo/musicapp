package pl.sarelo.data.network

interface ConnectionProvider {
    val isConnected: Boolean
}
