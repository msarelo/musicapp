package pl.sarelo.data.network.api

import org.json.JSONObject
import retrofit2.Response
import java.io.IOException

class RetrofitException private constructor(
    message: String?,
    /**
     * The request URL which produced the error.
     */
    val url: String?,
    private val response: Response<*>?,
    /**
     * The event kind which triggered this error.
     */
    val kind: Kind,
    exception: Throwable?,
    val httpStatusCode: Int,
    val detail: String?
) : RuntimeException(message, exception) {

    /**
     * Identifies the event kind which triggered a [RetrofitException].
     */
    enum class Kind {
        /**
         * An [IOException] occurred while communicating to the server.
         */
        NETWORK,
        /**
         * A non-200 HTTP status code was received from the server.
         */
        HTTP,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED,

        HANDLE_IN_APP
    }

    companion object {

        fun httpError(url: String, response: Response<*>, statusCode: Int): RetrofitException {
            val message = response.code().toString() + " " + response.message()
            val detail = getDetail(response)
            return RetrofitException(message, url, response, Kind.HTTP, null, statusCode, detail)
        }

        fun networkError(exception: IOException): RetrofitException {
            return RetrofitException(exception.message, null, null, Kind.NETWORK, exception, -1, null)
        }

        fun unexpectedError(exception: Throwable): RetrofitException {
            return RetrofitException(exception.message, null, null, Kind.UNEXPECTED, exception, -1, null)
        }

        fun errorHandleInApp(exception: Throwable): RetrofitException {
            return RetrofitException(exception.message, null, null, Kind.HANDLE_IN_APP, exception, -1, null)
        }

        private fun getDetail(response: Response<*>): String? {
            return try {
                val jObjError = JSONObject(response.errorBody()?.string())
                jObjError.getString("detail")
            } catch (exception: Throwable) {
                null
            }
        }
    }

    override fun toString(): String {
        return "RetrofitException(url=$url, response=$response, kind=$kind, httpStatusCode=$httpStatusCode, detail=$detail)"
    }
}
