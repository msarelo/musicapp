package pl.sarelo.data.network.requests

import io.reactivex.Single
import pl.sarelo.data.network.model.ItunesResult
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface SearchRequest {

    @GET("search")
    fun searchResult(@QueryMap queryMap: Map<String, String>): Single<ItunesResult>
}
