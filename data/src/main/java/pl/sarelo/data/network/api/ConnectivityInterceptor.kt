package pl.sarelo.data.network.api

import okhttp3.Interceptor
import okhttp3.Response
import pl.sarelo.data.network.ConnectionProvider
import pl.sarelo.data.network.ConnectivityException
import java.io.IOException

class ConnectivityInterceptor(private val connectionProvider: ConnectionProvider) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (connectionProvider.isConnected.not()) {
            throw ConnectivityException()
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }
}
