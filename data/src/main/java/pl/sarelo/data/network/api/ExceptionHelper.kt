package pl.sarelo.data.network.api

import com.orhanobut.logger.Logger
import io.reactivex.Single
import io.reactivex.SingleSource
import pl.sarelo.data.BuildConfig
import pl.sarelo.data.network.ConnectivityException
import retrofit2.HttpException
import java.io.IOException

class ExceptionHelper {

    companion object {
        @Throws(IOException::class)
        fun asRetrofitException(throwable: Throwable): SingleSource<Throwable> {
            return Single.error(handleException(throwable))
        }

        private fun handleException(throwable: Throwable) =
            when (throwable) {
                is HttpException -> throwable.response()?.run {
                    RetrofitException.httpError(raw().request().url().toString(), this, throwable.code())
                }
                is ConnectivityException -> RetrofitException.errorHandleInApp(throwable)
                is IOException -> RetrofitException.networkError(throwable)
                else -> RetrofitException.unexpectedError(throwable)
            }.also {
                if (BuildConfig.DEBUG) {
                    Logger.e("Network error: ", throwable)
                }
            }
    }
}
