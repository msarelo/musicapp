package pl.sarelo.data.network

import java.io.IOException

class ConnectivityException : IOException() {

    override fun fillInStackTrace(): Throwable {
        return this
    }
}
