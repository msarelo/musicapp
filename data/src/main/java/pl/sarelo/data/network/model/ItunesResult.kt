package pl.sarelo.data.network.model

data class ItunesResult(
    var resultCount: Int?,
    var results: List<ContentInfo?>?
)
