package pl.sarelo.data.network.mapper

import pl.sarelo.data.network.model.ContentInfo
import pl.sarelo.domain.entities.ArtistInfo
import pl.sarelo.domain.entities.MusicContent
import pl.sarelo.domain.entities.ReleaseInfo

fun ContentInfo.toMusicContent(dateFormatter: (date: String?) -> String): MusicContent {
    val artistInfo = ArtistInfo(artistName.orEmpty())
    val releaseInfo = ReleaseInfo(dateFormatter(releaseDate))
    return MusicContent(trackName.orEmpty(), collectionName.orEmpty(), artistInfo, releaseInfo)
}
