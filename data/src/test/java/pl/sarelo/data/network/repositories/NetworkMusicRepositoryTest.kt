package pl.sarelo.data.network.repositories

import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import pl.sarelo.data.network.model.ContentInfo
import pl.sarelo.data.network.model.ItunesResult
import pl.sarelo.data.network.requests.SearchRequest
import pl.sarelo.domain.entities.MusicContent

class NetworkMusicRepositoryTest {

    companion object {
        private lateinit var searchRequest: SearchRequest

        @BeforeClass
        @JvmStatic
        fun setup() {
            searchRequest = mockk()
        }
    }

    private lateinit var observer: TestObserver<List<MusicContent>>

    @Before
    fun beforeEachTest() {
        observer = TestObserver()
    }

    @Test
    fun `search should return list when API return items`() {
        // Given

        val mockedResponse = ItunesResult(10, (1..10).asSequence().map { mockk<ContentInfo>(relaxed = true) }.toList())
        every {
            searchRequest.searchResult(any())
        } returns Single.just(mockedResponse)

        val networkMusicRepository = NetworkMusicRepository(searchRequest)

        // When
        networkMusicRepository.search("", 0, 0).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            Assert.assertEquals("Size of result should be the same", it.values().first().size, 10)
        }
    }

    @Test
    fun `search should return exception when API don't return result`() {
        // Given
        every {
            searchRequest.searchResult(any())
        } returns Single.just(ItunesResult(10, null))

        val networkMusicRepository = NetworkMusicRepository(searchRequest)

        // When
        networkMusicRepository.search("", 0, 0).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertError(IllegalStateException::class.java)
        }
    }
}
