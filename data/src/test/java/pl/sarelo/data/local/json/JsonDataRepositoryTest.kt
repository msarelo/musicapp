package pl.sarelo.data.local.json

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import okio.BufferedSource
import org.junit.Assert.assertEquals
import org.junit.Test
import pl.sarelo.data.local.model.LocalMusicData
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.lang.reflect.Type

class JsonDataRepositoryTest {

    @Test
    fun `getAllData should return LocalMusicData records from json file`() {
        // Given
        val testJson =
            """
            [
                {
                    "Song Clean": "Waiting For The Sun",
                    "ARTIST CLEAN": "The Doors",
                    "Release Year": 1970,
                    "COMBINED": "Waiting For The Sun by The Doors",
                    "First?": 1,
                    "Year?": 1,
                    "PlayCount": 17,
                    "F*G": 17
                },
                {
                    "Song Clean": "When the Music's Over",
                    "ARTIST CLEAN": "The Doors",
                    "Release Year": 1967,
                    "COMBINED": "When the Music's Over by The Doors",
                    "First?": 1,
                    "Year?": 1,
                    "PlayCount": 1,
                    "F*G": 1
                }
            ]
            """
        val inputStream = ByteArrayInputStream(testJson.toByteArray(Charsets.UTF_8))
        val repository = JsonDataRepository(inputStream, Moshi.Builder().build(), LocalMusicData::class.java)

        // When
        val output = repository.getAllData().blockingGet()

        // Then
        assertEquals("Size should be equals", output.size, 2)
        assertEquals("Objects should be instance of", output.first()::class.java, LocalMusicData::class.java)
        assertEquals("Name should be the same", output.first().artistClean, "The Doors")
    }

    @Test
    fun `getAllData should return SomeTest records from json file`() {
        // Given
        val testJson =
            """
            [
                {
                  "field1": "object 1 value 1",
                  "field2": "object 1 value 2"
                },
                {
                  "field1": "object 2 value 1",
                  "field1": "object 2 value 2"
                }
            ]
            """
        val inputStream = ByteArrayInputStream(testJson.toByteArray(Charsets.UTF_8))
        val repository = JsonDataRepository(inputStream, Moshi.Builder().build(), SomeTest::class.java)

        // When
        val output = repository.getAllData().blockingGet()

        // Then
        assertEquals("Size should be equals", output.size, 2)
        assertEquals("Objects should be instance of", output.first()::class.java, SomeTest::class.java)
    }

    @Test
    fun `getAllData called twice should on second time load data from cache`() {
        // Given
        val moshi = mockk<Moshi>()
        val adapter = mockk<JsonAdapter<List<String>>>()

        every {
            moshi.adapter<List<String>>(any<Type>())
        } returns adapter

        every {
            adapter.fromJson(any<BufferedSource>())
        } returns listOf("a", "b")

        val inputStream = mockk<InputStream>(relaxed = true)
        val repository = JsonDataRepository(inputStream, moshi, String::class.java)

        // When
        val output1 = repository.getAllData().blockingGet()
        val output2 = repository.getAllData().blockingGet()

        // Then
        assertEquals("Size should be the same of each output", output1.size, output2.size)
        assertEquals("Both outputs should be the same", output1, output2)
        verify(exactly = 1) { moshi.adapter<List<String>>(any<Type>()) }
        verify(exactly = 1) { adapter.fromJson(any<BufferedSource>()) }
    }

    data class SomeTest(val field1: String, val field2: String)
}
