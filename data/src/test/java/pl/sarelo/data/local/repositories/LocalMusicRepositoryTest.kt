package pl.sarelo.data.local.repositories

import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import pl.sarelo.data.local.DataRepository
import pl.sarelo.data.local.model.LocalMusicData
import pl.sarelo.domain.entities.MusicContent

class LocalMusicRepositoryTest {

    private lateinit var observer: TestObserver<List<MusicContent>>

    companion object {
        private lateinit var dataRepository: DataRepository<LocalMusicData>

        @BeforeClass
        @JvmStatic
        fun setup() {
            dataRepository = mockk()
        }
    }

    @Before
    fun beforeEachTest() {
        observer = TestObserver()
    }

    @Test
    fun `searching given term {Maria} should return item`() {
        // Given
        val mockedResponse = listOf(
            LocalMusicData(
                "Maria",
                "Maria a",
                1,
                1,
                1,
                "200",
                "Jestem",
                1
            ),
            LocalMusicData(
                "Edyta",
                "Edyta a",
                1,
                1,
                1,
                "200",
                "Nie ma mnie",
                1
            )

        )

        every {
            dataRepository.getAllData()
        } returns Single.just(mockedResponse)

        val localMusicRepository = LocalMusicRepository(dataRepository)

        // When
        localMusicRepository.search("Maria", 0, 5).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals("Size of result should be the same", it.values().first().size, 1)
            assertEquals(
                "Name should be Maria",
                "Maria",
                it.values().first().first().artistInfo.artistName
            )
        }
    }

    @Test
    fun `searching given term {Nie ma mnie} should return item`() {
        // Given
        val mockedResponse = listOf(
            LocalMusicData(
                "Maria",
                "Maria a",
                1,
                1,
                1,
                "200",
                "Jestem",
                1
            ),
            LocalMusicData(
                "Edyta",
                "Edyta a",
                1,
                1,
                1,
                "200",
                "Nie ma mnie",
                1
            )

        )

        every {
            dataRepository.getAllData()
        } returns Single.just(mockedResponse)

        val localMusicRepository = LocalMusicRepository(dataRepository)

        // When
        localMusicRepository.search("Nie ma mnie", 0, 5).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals("Size of result should be the same", it.values().first().size, 1)
            assertEquals(
                "Name should be Edyta",
                "Edyta",
                it.values().first().first().artistInfo.artistName
            )
        }
    }

    @Test
    fun `search for 10 items from empty list should return empty list`() {
        // Given
        every {
            dataRepository.getAllData()
        } returns Single.just(emptyList())

        val localMusicRepository = LocalMusicRepository(dataRepository)

        // When
        localMusicRepository.search("artistClean", 0, 10).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals("Size of result should be the same", it.values().first().size, 0)
        }
    }

    @Test
    fun `search for 10 items from list which contains 5 should return 5 items `() {
        // Given
        val range = 0..10
        val mockedResponse = (0..4).asSequence().map { index ->
            mockk<LocalMusicData> {
                every { artistClean } returns "artistClean $index"
                every { songClean } returns "songClean"
                every { year } returns 1
                every { releaseYear } returns "2000"

                every { containString(any()) } returns range.contains(index)
            }
        }.toList()

        every {
            dataRepository.getAllData()
        } returns Single.just(mockedResponse)

        val localMusicRepository = LocalMusicRepository(dataRepository)

        // When
        localMusicRepository.search("artistClean", 0, 10).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals("Size of result should be the same", it.values().first().size, 5)
        }
    }

    @Test
    fun `search for 5 from first items should return 5 items from first page`() {
        // Given
        val range = 0..5
        val mockedResponse = (0..10).asSequence().map { index ->
            mockk<LocalMusicData> {
                every { artistClean } returns "artistClean $index"
                every { songClean } returns "songClean"
                every { year } returns 1
                every { releaseYear } returns "2000"

                every { containString(any()) } returns range.contains(index)
            }
        }.toList()

        every {
            dataRepository.getAllData()
        } returns Single.just(mockedResponse)

        val localMusicRepository = LocalMusicRepository(dataRepository)

        // When
        localMusicRepository.search("artistClean", 0, 5).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals("Size of result should be the same", it.values().first().size, 5)
            assertEquals(
                "First item should contain index 0",
                "0",
                it.values().first().first().artistInfo.artistName.lastChar()
            )
            assertEquals(
                "Last item in first page should contain index 4",
                "4",
                it.values().first().last().artistInfo.artistName.lastChar()
            )
        }
    }

    @Test
    fun `with given parameters search should return items from index 0 to 22 {23 items}`() {
        // Given
        val range = 0..22
        val mockedResponse = (0..100).asSequence().map { index ->
            mockk<LocalMusicData> {
                every { artistClean } returns "artistClean $index"
                every { songClean } returns "songClean"
                every { year } returns 1
                every { releaseYear } returns "2000"

                every { containString(any()) } returns range.contains(index)
            }
        }.toList()

        every {
            dataRepository.getAllData()
        } returns Single.just(mockedResponse)

        val localMusicRepository = LocalMusicRepository(dataRepository)

        // When
        localMusicRepository.search("artistClean", 0, 23).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals("Size of result should be the same", it.values().first().size, 23)
            assertEquals(
                "First item should contain index 0",
                "0",
                it.values().first().first().artistInfo.artistName.lastChar()
            )
            assertEquals(
                "Last item in first page should contain index 22",
                "22",
                it.values().first().last().artistInfo.artistName.lastTwoChars()
            )
        }
    }

    @Test
    fun `with given parameters search should return items from index 90 to 99 {10 items}`() {
        // Given
        val range = 90..99
        val mockedResponse = (0..100).asSequence().map { index ->
            mockk<LocalMusicData> {
                every { artistClean } returns "artistClean $index"
                every { songClean } returns "songClean"
                every { year } returns 1
                every { releaseYear } returns "2000"

                every { containString(any()) } returns range.contains(index)
            }
        }.toList()

        every {
            dataRepository.getAllData()
        } returns Single.just(mockedResponse)

        val localMusicRepository = LocalMusicRepository(dataRepository)

        // When
        localMusicRepository.search("artistClean", 0, 10).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals("Size of result should be the same", it.values().first().size, 10)
            assertEquals(
                "First item should contain index 90",
                "90",
                it.values().first().first().artistInfo.artistName.lastTwoChars()
            )
            assertEquals(
                "Last item in first page should contain index 99",
                "99",
                it.values().first().last().artistInfo.artistName.lastTwoChars()
            )
        }
    }

    @Test
    fun `with given parameters search should return items from index 34 to 45 {12}`() {
        // Given
        val range = IntRange(22, 50)
        val mockedResponse = (0..100).asSequence().map { index ->
            mockk<LocalMusicData> {
                every { artistClean } returns "artistClean $index"
                every { songClean } returns "songClean"
                every { year } returns 1
                every { releaseYear } returns "2000"

                every { containString(any()) } returns range.contains(index)
            }
        }.toList()

        every {
            dataRepository.getAllData()
        } returns Single.just(mockedResponse)

        val localMusicRepository = LocalMusicRepository(dataRepository)

        // When
        localMusicRepository.search("artistClean", 1, 12).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals("Size of result should be the same", it.values().first().size, 12)
            assertEquals(
                "First item should contain index 34",
                "34",
                it.values().first().first().artistInfo.artistName.lastTwoChars()
            )
            assertEquals(
                "Last item in first page should contain index 45",
                "45",
                it.values().first().last().artistInfo.artistName.lastTwoChars()
            )
        }
    }
}

private fun String.lastChar() = substring(length - 1, length)
private fun String.lastTwoChars() = substring(length - 2, length)
