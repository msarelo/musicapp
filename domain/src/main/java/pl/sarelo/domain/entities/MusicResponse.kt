package pl.sarelo.domain.entities

class MusicResponse(
    val data: List<MusicContent> = emptyList(),
    val errorLocal: Throwable? = null,
    val errorNetwork: Throwable? = null
)
