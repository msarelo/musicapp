package pl.sarelo.domain.entities

sealed class SearchRequest(val searchTerm: String, val pageNumber: Int, val itemsPerPage: Int) {

    class Network(searchTerm: String, pageNumber: Int, itemsPerPage: Int) :
        SearchRequest(searchTerm, pageNumber, itemsPerPage)

    class Local(searchTerm: String, pageNumber: Int, itemsPerPage: Int) :
        SearchRequest(searchTerm, pageNumber, itemsPerPage)

    class Both(searchTerm: String, pageNumber: Int, itemsPerPage: Int) :
        SearchRequest(searchTerm, pageNumber, itemsPerPage)
}
