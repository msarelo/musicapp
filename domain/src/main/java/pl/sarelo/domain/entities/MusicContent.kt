package pl.sarelo.domain.entities

data class MusicContent(
    val title: String,
    val albumName: String,
    val artistInfo: ArtistInfo,
    val releaseInfo: ReleaseInfo
)
