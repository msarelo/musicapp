package pl.sarelo.domain.entities

data class ReleaseInfo(val releaseDate: String)
