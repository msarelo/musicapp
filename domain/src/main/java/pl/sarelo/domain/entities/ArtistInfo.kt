package pl.sarelo.domain.entities

data class ArtistInfo(val artistName: String)
