package pl.sarelo.domain.usecases

import io.reactivex.Single
import io.reactivex.SingleTransformer
import pl.sarelo.domain.SchedulerProvider

class SingleExecuteAndObserveTransformer<T>(private val schedulerProvider: SchedulerProvider) :
    SingleTransformer<T, T> {
    override fun apply(upstream: Single<T>): Single<T> =
        upstream
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.mainThread())
}
