package pl.sarelo.domain.usecases

import io.reactivex.Single
import io.reactivex.functions.BiFunction
import pl.sarelo.domain.SchedulerProvider
import pl.sarelo.domain.entities.MusicResponse
import pl.sarelo.domain.entities.SearchRequest
import pl.sarelo.domain.repositories.MusicRepository

class SearchMusicUseCase(
    private val networkMusicRepository: MusicRepository,
    private val localMusicRepository: MusicRepository,
    schedulerProvider: SchedulerProvider
) : UseCaseWithParamsSingle<SearchRequest, MusicResponse>(schedulerProvider) {

    @Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
    override fun buildUseCase(searchRequest: SearchRequest): Single<MusicResponse> {
        return when (searchRequest) {
            is SearchRequest.Network -> getDataFromNetwork(searchRequest)
            is SearchRequest.Local -> getDataFromLocal(searchRequest)
            is SearchRequest.Both -> {
                return Single.zip(
                    getDataFromNetwork(searchRequest), getDataFromLocal(searchRequest),
                    BiFunction { network, local ->
                        val data = network.data.toMutableList().apply { addAll(local.data) }
                        MusicResponse(data, local.errorLocal, network.errorNetwork)
                    })
            }
        }
    }

    private fun getDataFromLocal(searchRequest: SearchRequest) =
        localMusicRepository.search(searchRequest.searchTerm, searchRequest.pageNumber, searchRequest.itemsPerPage)
            .map { MusicResponse(data = it) }
            .onErrorReturn { MusicResponse(errorLocal = it) }

    private fun getDataFromNetwork(searchRequest: SearchRequest) =
        networkMusicRepository.search(searchRequest.searchTerm, searchRequest.pageNumber, searchRequest.itemsPerPage)
            .map { MusicResponse(data = it) }
            .onErrorReturn { MusicResponse(errorNetwork = it) }
}
