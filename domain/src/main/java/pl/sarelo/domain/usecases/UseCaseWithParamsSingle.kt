package pl.sarelo.domain.usecases

import io.reactivex.Single
import pl.sarelo.domain.SchedulerProvider

abstract class UseCaseWithParamsSingle<P, T>(private val schedulerProvider: SchedulerProvider) {

    internal abstract fun buildUseCase(useCaseParam: P): Single<T>

    fun execute(useCaseParam: P): Single<T> =
        buildUseCase(useCaseParam).compose(SingleExecuteAndObserveTransformer(schedulerProvider))
}
