package pl.sarelo.domain.repositories

import io.reactivex.Single
import pl.sarelo.domain.entities.MusicContent

interface MusicRepository {
    fun search(searchTerm: String, pageNumber: Int, itemsPerPage: Int): Single<List<MusicContent>>

    fun getStartIndex(pageNumber: Int, itemsPerPage: Int): Int {
        require(pageNumber >= 0) { "pageNumber should have positive value or equals 0" }
        require(itemsPerPage >= 1) { "itemsPerPage should be greater than 0" }
        return pageNumber * itemsPerPage
    }

    fun getEndIndex(pageNumber: Int, itemsPerPage: Int): Int {
        require(pageNumber >= 0) { "pageNumber should have positive value or equals 0" }
        require(itemsPerPage >= 1) { "itemsPerPage should be greater than 0" }

        return if (pageNumber == 0) {
            itemsPerPage - 1
        } else {
            ((pageNumber + 1) * itemsPerPage) - 1
        }
    }
}
