package pl.sarelo.domain.repositories

import io.reactivex.Single
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import pl.sarelo.domain.entities.MusicContent

@RunWith(Parameterized::class)
class MusicRepositoryGetStartIndexTest(
    private val pageNumber: Int,
    private val itemsPerPage: Int,
    private val expectedValue: Int,
    private val expectedException: Class<out Exception>?,
    private val expectedExceptionMsg: String?
) {

    @Rule
    @JvmField
    var thrown: ExpectedException = ExpectedException.none()

    companion object {

        @JvmStatic
        @Parameterized.Parameters(name = "{index}: getStartIndex({0},{1})={2} or exception {3} with {4}")
        fun data(): List<Array<out Any?>> {
            return listOf(
                arrayOf(0, 0, 0, IllegalArgumentException::class.java, "itemsPerPage should be greater than 0"),
                arrayOf(1, 1, 1, null, ""),
                arrayOf(1, 2, 2, null, ""),
                arrayOf(1, 20, 20, null, ""),
                arrayOf(0, 10, 0, null, ""),
                arrayOf(3, 2, 6, null, ""),
                arrayOf(3, 7, 21, null, ""),
                arrayOf(
                    -1,
                    7,
                    21,
                    IllegalArgumentException::class.java,
                    "pageNumber should have positive value or equals 0"
                ),
                arrayOf(1, -7, 21, IllegalArgumentException::class.java, "itemsPerPage should be greater than 0")
            )
        }
    }

    private val underTest = object : MusicRepository {
        override fun search(searchTerm: String, pageNumber: Int, itemsPerPage: Int): Single<List<MusicContent>> {
            return Single.just(emptyList())
        }
    }

    @Test
    fun `should return correct`() {
        // Given
        expectedException?.let {
            thrown.expect(it)
            thrown.expectMessage(expectedExceptionMsg)
        }

        // When
        val output = underTest.getStartIndex(pageNumber, itemsPerPage)

        // Then
        Assert.assertEquals("values should be the same:$expectedValue and $output", expectedValue, output)
    }
}
