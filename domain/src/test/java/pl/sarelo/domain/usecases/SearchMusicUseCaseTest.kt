package pl.sarelo.domain.usecases

import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Test
import pl.sarelo.domain.SchedulerProvider
import pl.sarelo.domain.entities.MusicContent
import pl.sarelo.domain.entities.MusicResponse
import pl.sarelo.domain.entities.SearchRequest
import pl.sarelo.domain.repositories.MusicRepository

class SearchMusicUseCaseTest {

    @Test
    fun `output of usecase should return lists from network repository`() {
        // Given
        val observer = TestObserver<MusicResponse>()

        val local = mockk<MusicRepository>()
        every {
            local.search(any(), any(), any())
        } returns Single.just((1..10).asSequence().map { mockk<MusicContent>() }.toList())

        val network = mockk<MusicRepository>()
        every {
            network.search(any(), any(), any())
        } returns Single.just((1..100).asSequence().map { mockk<MusicContent>() }.toList())

        val searchMusicUseCase = SearchMusicUseCase(network, local, object : SchedulerProvider {
            override fun io() = Schedulers.trampoline()
            override fun mainThread() = Schedulers.trampoline()
        })

        // When
        searchMusicUseCase.execute(SearchRequest.Network("", 0, 5)).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals(it.values().first().data.size, 100)
        }
    }

    @Test
    fun `output of usecase should return lists from local repository`() {
        // Given
        val observer = TestObserver<MusicResponse>()

        val local = mockk<MusicRepository>()
        every {
            local.search(any(), any(), any())
        } returns Single.just((1..10).asSequence().map { mockk<MusicContent>() }.toList())

        val network = mockk<MusicRepository>()
        every {
            network.search(any(), any(), any())
        } returns Single.just((1..100).asSequence().map { mockk<MusicContent>() }.toList())

        val searchMusicUseCase = SearchMusicUseCase(network, local, object : SchedulerProvider {
            override fun io() = Schedulers.trampoline()
            override fun mainThread() = Schedulers.trampoline()
        })

        // When
        searchMusicUseCase.execute(SearchRequest.Local("", 0, 5)).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals(it.values().first().data.size, 10)
        }
    }

    @Test
    fun `output of usecase should return empty list and error when ask for network data when error occur in network stream `() {
        // Given
        val observer = TestObserver<MusicResponse>()

        val local = mockk<MusicRepository>()
        every {
            local.search(any(), any(), any())
        } returns Single.just((1..10).asSequence().map { mockk<MusicContent>() }.toList())

        val network = mockk<MusicRepository>()
        every {
            network.search(any(), any(), any())
        } returns Single.error(NullPointerException(""))

        val searchMusicUseCase = SearchMusicUseCase(network, local, object : SchedulerProvider {
            override fun io() = Schedulers.trampoline()
            override fun mainThread() = Schedulers.trampoline()
        })

        // When
        searchMusicUseCase.execute(SearchRequest.Network("", 0, 5)).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals(it.values().first().data.size, 0)
            assertEquals(it.values().first().errorNetwork!!::class.java, NullPointerException::class.java)
        }
    }

    @Test
    fun `output of usecase should return data only from local repository and error when error occur in network stream when ask form data from both repositories`() {
        // Given
        val observer = TestObserver<MusicResponse>()

        val local = mockk<MusicRepository>()
        every {
            local.search(any(), any(), any())
        } returns Single.just((1..10).asSequence().map { mockk<MusicContent>() }.toList())

        val network = mockk<MusicRepository>()
        every {
            network.search(any(), any(), any())
        } returns Single.error(NullPointerException(""))

        val searchMusicUseCase = SearchMusicUseCase(network, local, object : SchedulerProvider {
            override fun io() = Schedulers.trampoline()
            override fun mainThread() = Schedulers.trampoline()
        })

        // When
        searchMusicUseCase.execute(SearchRequest.Both("", 0, 5)).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals(it.values().first().data.size, 10)
            assertEquals(it.values().first().errorNetwork!!::class.java, NullPointerException::class.java)
        }
    }

    @Test
    fun `output of usecase should merge lists from repositories`() {
        // Given
        val observer = TestObserver<MusicResponse>()

        val local = mockk<MusicRepository>()
        every {
            local.search(any(), any(), any())
        } returns Single.just((1..10).asSequence().map { mockk<MusicContent>() }.toList())

        val network = mockk<MusicRepository>()
        every {
            network.search(any(), any(), any())
        } returns Single.just((1..100).asSequence().map { mockk<MusicContent>() }.toList())

        val searchMusicUseCase = SearchMusicUseCase(network, local, object : SchedulerProvider {
            override fun io() = Schedulers.trampoline()
            override fun mainThread() = Schedulers.trampoline()
        })

        // When
        searchMusicUseCase.execute(SearchRequest.Both("", 0, 5)).subscribe(observer)

        // Then
        observer.assertOf {
            it.assertNoErrors()
            it.assertComplete()
            it.assertValueCount(1)

            assertEquals(it.values().first().data.size, 110)
        }
    }
}
